"use strict";
jQuery(document).ready(function($) {
    // Window on load
    $('.banner-slide .owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: true,
        autoplay: true,
        autoplayTimeout: 5000,
        navText: ["<i class=\"fas fa-angle-left\"></i>", "<i class=\"fas fa-angle-right\"></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    $('.menu__tab-product .owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: true,
        navText: ["<i class=\"fas fa-angle-left\"></i>", "<i class=\"fas fa-angle-right\"></i>"],
        responsive: {
            0: {
                items: 1
            },
            482: {
                items: 2
            },
            767: {
                items: 3
            },
            992: {
                items: 4
            }
        }
    });
    $('.product-sale__slide .owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: false,
        navText: ["<i class=\"fas fa-angle-left\"></i>", "<i class=\"fas fa-angle-right\"></i>"],
        responsive: {
            0: {
                items: 1
            },
            482: {
                items: 2
            },
            767: {
                items: 3
            },
            992: {
                items: 4
            }
        }
    });
    $('.customer-cmt__slide .owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: true,
        autoplay: true,
        autoplayTimeout: 10000,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    $('.partner__list .owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 5
            },
            600: {
                items: 5
            },
            1000: {
                items: 5
            }
        }
    });
    $('.image-zoom .owl-carousel').owlCarousel({
        loop: true,
        animateOut: 'fadeOut',
        animateIn: 'fadaIn',
        margin: 10,
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    $('.nav-thuml .owl-carousel').owlCarousel({
        loop: false,
        margin: 10,
        nav: true,
        dots: false,
        center: true,
        mouseDrag: false,
        navText: ["<i class=\"fas fa-angle-left\"></i>", "<i class=\"fas fa-angle-right\"></i>"],
        responsive: {
            0: {
                items: 4
            },
            600: {
                items: 4
            },
            1000: {
                items: 4
            }
        }
    });

    $('.lst-item .owl-carousel').owlCarousel({
        loop: false,
        margin: 10,
        nav: true,
        dots: true,
        center: true,
        mouseDrag: true,
        navText: ["<i class=\"fa fa-caret-left\"></i>", "<i class=\"fa fa-caret-right\"></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    //menu-fix
    if ($('.bar-menu').length != 0) {
        $('.bar-menu').click(function() {
            $('#wrapper').toggleClass('show');
            $('.header-fixed').toggleClass('show');
        })
    };
    if ($('.menu-mobie').length != 0) {
        $('.menu-mobie').click(function() {
            $('#wrapper').toggleClass('show');
            $('.header-fixed').toggleClass('show');
        })
    }
    if ($('#cart-form').length != 0) {
        $('td.remove a').click(function() {
            $(this).closest('tr').removeClass('active');
        })
    }
    if ($('.view-product-popup').length != 0) {
        $('.view-product-popup .nav-thuml .owl-item img').click(function() {
            $('.view-product-popup .nav-thuml .owl-item img').removeClass('active');
            $(this).addClass('active');
        })
    }
    if ($('.drop-button').length != 0) {
        $('.drop-button').click(function() {
            $('.menu-left__2').slideToggle();
            $('li.list-drop').toggleClass('active');
            $(this).toggleClass('active');
        });
    }
    $("#user-form").validate({
        rules: {
            info_name: "required",
            info_number: {
                required: true,
                number: true,
                minlength: 8,
                maxlength: 13,
            },
            info_email: {
                required: true,
                email: true
            },
            info_address: {
                required: true
            },
        },
        messages: {
            info_name: {
                required: "Phải nhập họ tên"
            },
            info_number: {
                required: "Phải nhập số điện thoại",
                number: "Số điện thoại chưa chính xác",
                minlength: "Số điện thoại từ 8 đến 13 số",
                maxlength: "Số điện thoại từ 8 đến 13 số"
            },
            info_email: {
                required: "Phải nhập email",
                email: "Email chưa chính xác"
            },
            info_address: {
                required: "Phải nhập Địa chỉ",
            }
        }
    });
    if ($('.checkout-page .box-cart').length != 0) {
        $('a.btn-coupon').click(function() {
            $('.box-cart .coupon').addClass('active');
        });
    };

    //info-product-slider
    $(".zoom").elevateZoom({
        zoomType: "lens",
        lensShape: "round",
        lensSize: 200
    });
    $('.zoom').on('click', function() {
        var imageUrl = $(this).data('zoom-image');

        console.log(imageUrl);

        var imageHtml = '<img src="' + imageUrl + '">'

        var zoomImg = $(this).append(imageHtml);

        console.log($(this).append(imageHtml).width());
    });

    //info-product-slider

    $('.quantity-up').on('click', function() {
        var elem = $(this).closest('.quantity').find('.quantity-val');
        var val = Number(elem.val()) + 1;
        elem.val(val);
    });

    $('.quantity-down').on('click', function() {
        var elem = $(this).closest('.quantity').find('.quantity-val');
        var val = Number(elem.val()) - 1;
        if (val < 1) val = 1;
        elem.val(val);
    });

});
$(document).ready(function() {
    $(window).scroll(function() {
            $(this).scrollTop() > 360 ? $(".up").addClass("up-active") : $(".up").removeClass("up-active")
        }),
        $(".up").click(function() {
            return $("html,body").animate({
                scrollTop: 0
            }, 450), !1
        })
    
    $(".toolbar a.large").click(function(){
        $(".menu-content").show();
        $(".menu-content2").hide();   
        return false;     
    })

    $(".toolbar a.list").click(function(){
        $(".menu-content2").show();
        $(".menu-content").hide();      
        return false;  
    })

    $(window).load(function() {
        // The slider being synced must be initialized first
        $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 210,
            itemMargin: 5,
            asNavFor: '#slider',
            minItems: 3
        });
    
        $('#slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel",
        });
    });
    $("#dang_nhap").validate({
        rules: {
            login_email:{
                required: true,
                email: true
            },
            login_pass: {
                required: true,
                minlength: 4
            }
        },
        messages: {
            login_pass: {
                required: "Chưa nhập mật khẩu",
                minlength: "mật khẩu có ít nhất 4 kí tự"
            },
            login_email: {
                required: "Phải nhập email",
                email: "Email chưa chính xác"
            }
        }
    });
    $("#dang_ki").validate({
        rules: {
            singin_first_name:{
                required: true
            },
            singin_name:{
                required: true
            },
            singin_email:{
                required: true,
                email: true
            },
            singin_pass: {
                required: true,
                minlength: 4
            }
        },
        messages: {
            singin_first_name:{
                required:"Không được để trống"
            },
            singin_name:{
                required: "Không được để trống"
            },
            singin_email: {
                required: "Phải nhập email",
                email: "Email chưa chính xác"
            },
            singin_pass: {
                required: "Chưa nhập mật khẩu",
                minlength: "mật khẩu có ít nhất 4 kí tự"
            },
        }
    });
    if ($('.listmenu2').length != 0) {
        $('.listmenu2 .arrow-down').click(function(){
            $(this).closest('li').find('.list-menu__2').slideToggle();
            return false;
        });
    };   
});


